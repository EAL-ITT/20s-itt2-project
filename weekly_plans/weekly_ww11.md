---
Week: 11
Content:  consolidation 3/3
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 11

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Particle cloud publish and variables tested
* Delayed parts of the project completed/scheduled

### Learning goals

* Particle cloud
  * Level 1: The student is able to publish simple messages to the particle cloud
  * Level 2: The student is able to read variables from particle cloud
  * Level 3: The student is able to read published messages and variables from node-red

## Deliverables

* Daily group morning meeting - see week 05 for agenda
* Mandatory weekly meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
    5. Any other business (AOB)
* Exercise documentation included in Gitlab project

## Schedule

### Monday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Introduction to the day, general Q&A session, decide on extra session if applicable |
|       | Group morning meetings |
| 9:00  | Teacher meetings - see agenda|
| 9:00  | Hands-on time (you work on exercises) |
| 15:30 | End of day |


### Tuesday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Introduction to the day, general Q&A session, decide on extra session if applicable |
|       | Group morning meetings |
| 9:00  | Hands-on time (you work on exercises) |
| 15:30 | End of day |

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/20s-itt2-project/20S_ITT2_exercises.pdf) for exercises details

## Comments
* Companion site [https://eal-itt.gitlab.io/iot-with-particle/](https://eal-itt.gitlab.io/iot-with-particle/)

---
Week: 10
Content:  consolidation 2/3
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 10

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* gitlab pipelines are running and tests performed

### Learning goals

* Automated documetation
  * Level 1: Can explain .gitlab-ci.yml file
  * Level 2: Can use pipelines to publish pages
  * Level 3: Implement automated documentation and publish

## Deliverables

* Daily group morning meeting - see week 05 for agenda
* Mandatory weekly meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
    5. Any other business (AOB)
* Exercise documentation included in Gitlab project

## Schedule

### Monday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Introduction to the day, general Q&A session, decide on extra session if applicable |
|       | Group morning meetings |
| 9:00  | Teacher meetings - see agenda|
| 9:00  | Hands-on time (you work on exercises) |
| 15:30 | End of day |


### Tuesday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Introduction to the day, general Q&A session, decide on extra session if applicable |
|       | Group morning meetings |
| 9:00  | Hands-on time (you work on exercises) |
| 15:30 | End of day |


### Thursday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Group morning meetings |
| 15:30 | End of day |

Notice that the teachers are absent on Thursday. MON will be monitoring riot, so ask questions there.

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/20s-itt2-project/20S_ITT2_exercises.pdf) for exercises details

## Comments
* Companion site [https://eal-itt.gitlab.io/iot-with-particle/](https://eal-itt.gitlab.io/iot-with-particle/)
* Thursday we expect you to work by yourselves. This includes the group meeting.

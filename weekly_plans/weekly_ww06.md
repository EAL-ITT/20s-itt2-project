---
Week: 06
Content:  proof of concept 1/3
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 06

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Initial project documentation is made and on gitlab
* Initial Boron tool chain is working and LED blinking.
* Initial Boron to RPi UART communication working
* Collaboration with CS group established, if appl.

### Learning goals

* Development platform toolchain
  * Level 1: Able to push programs from cloud to Boron 
  * Level 2: Able to push programs from workbench to Boron 
  * Level 3: Able to manage and update firmware and programs locally

* Basic Boron local interfacing
  * Level 1: Able to describe UART serial communication
  * Level 2: Able to set up and use Boron serial communication
  * Level 3: Able to debug serial connection (Linux + electrically)

* Collaboration between groups of different professions
  * Level 1: Able to describe the project you are working on to a different profession
  * Level 2: Able to evaluate if a collabration is useful for the group
  * Level 3: Able to establish a serious collaboration with a different profession

## Deliverables

* Updated project plan included in Gitlab project
* Daily group morning meeting - see week 05 for agenda
* Mandatory weekly meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
    5. Any other business (AOB)
* Exercise documentation included in Gitlab project

## Schedule

### Monday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Introduction to the day, general Q&A session, decide on extra session if applicable |
|       | Group morning meetings |
| 9:00  | Teacher meetings - see agenda|
| 9:00  | Hands-on time (you work on exercises) |
| 15:30 | End of day |


### Tuesday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Introduction to the day, general Q&A session, decide on extra session if applicable |
|       | Group morning meetings |
| 9:00  | Hands-on time (you work on exercises) |
| 12:15 | Meeting with CS groups - room B2.05/B2.49|
|       | 2 students from each group attends a meeting with computer science students to arrange for a possible collaboration.
|       | We suggest you talk about:
|       | What you are working on in the project in order to investigate how you can collaborate
|       | Agree on next steps in the collabration (ie. when is the next meeting ?)
|       | Exchange communication channels (Riot, gitlab etc.)
| 15:30 | End of day |

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/20s-itt2-project/20S_ITT2_exercises.pdf) for exercises details

## Comments
* Make sure to check out the brand spanking new project companion site [https://eal-itt.gitlab.io/iot-with-particle/](https://eal-itt.gitlab.io/iot-with-particle/)

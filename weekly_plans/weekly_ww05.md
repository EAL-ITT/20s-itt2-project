---
Week: 05
Content:  Project startup
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 05

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Groups formed
* Gitlab project created
* Project plan updated

### Learning goals
* Basic gitlab project management
  * Level 1: Know what gitlab.com is in a project management context.
  * Level 2: Able to set up and use issues in gitlab.com
  * Level 3: Able to use gitlab.com for project management

* Basic project definitions and description
  * Level 1: Know the important elements when defining projects
  * Level 2: Able to define a project.
  * Level 3: Able to define a project and use it as part of running the project

## Deliverables
* Mandatory weeky meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)

* Initial group meetinga
  * agenda is
    1. Group members introduce themselves (use [round table](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf#section.3))
    2. Communication channels within the group
    3. Ambitions within the group (use round table)
    4. Aob

* Updated project plan included in Gitlab project

* Daily group morning meeting
  * Generic agenda (feel free to adapt):
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.
    4. Aob

## Schedule

### Monday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Introduction to the project part 1 |
| 9:00  | Forming teams - exercise 1|
| 9:30  | First group meeting - remember to do minutes of meeting |
| 10:30 | Create gitlab project - exercise 2 |
| 13:00 | Company presentation (auditorium) - Hydac A/S - IoT Monitoring of hydraulic pumps \
          It is a requirement that you take notes during the presentation
| 15:30 | End of day

### Tuesday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Introduction to the day, general Q&A session |
| 8:45  | Group morning meeting  
| 9:00  | pre-mortem - exercise 3 |
| 10:30 | Read up on Hydac sensor documentation and particle.io \
          You must build an overview of capabilities and options. |
| 12:30 | Update project plan - exercise 4 |
| 15:30 | End of day



## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/20s-itt2-project/20S_ITT2_exercises.pdf) for exercises details

## Comments
* we will have meetings with computer science students next week.

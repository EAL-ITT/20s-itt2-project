---
Week: 22
Content:  part 2 - Make it useful 1/3
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 22

For this last phase of part 2, you are to make your system useful for your users.

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

None at the moment

### Learning goals

None at the moment

## Deliverables

* Daily group morning meeting - see week 05 for agenda
* Mandatory weekly meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
    5. Any other business (AOB)

* Exercise documentation included in Gitlab project

## Schedule


### Monday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Meeting room [https://ucldk.zoom.us/join](https://ucldk.zoom.us/join) Meeting ID: **615 3388 1974** Password: **123456** is open, teachers available as much as possible |
|       | Guide on how to use zoom are in the comments below |
| 9:00  | Introduction to the day, general Q&A session, decide on extra session if applicable |
|       | **During this meeting please keep your camera *enabled* and microphone *disabled*** |
|       | To join the video meeting, click this link: [https://ucldk.zoom.us/join](https://ucldk.zoom.us/join) Meeting ID: **615 3388 1974** Password: **123456** |
|       | Video recording from the meeting: [google docs](https://drive.google.com/open?id=19dQX2I9OXAxOtPaxiEyCra5ttVA3uQc5) |
| 9:30  | Group morning meetings - use your virtual group room |
| 10:00 | Hands-on time (you work on exercises) |
| 12:00 | Teacher meetings - see agenda and timetable |
| 15:00 | Q&A and feedback |
|       | In the common room, [https://ucldk.zoom.us/join](https://ucldk.zoom.us/join) Meeting ID: **615 3388 1974** Password: **123456**, everybody joins a Q&A + feedback session.
|       | We will talk about experiences during this weeks project and give each other feedback |
|       | Specific agenda will be presented at the meeting |
| 15:30 | End of day |

### Tuesday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Meeting room [https://ucldk.zoom.us/join](https://ucldk.zoom.us/join) Meeting ID: **615 3388 1974** Password: **123456** is open, teachers probably not available much |
| 9:00  | Introduction to the day, general Q&A session, decide on extra session if applicable |
|       | Topics to be touched upon unittesting, after-easter plan, docs quality comments |    |
|       | **During this meeting please keep your camera *enabled* and microphone *disabled*** |
|       | To join the video meeting, click this link: [https://ucldk.zoom.us/join](https://ucldk.zoom.us/join) Meeting ID: **615 3388 1974** Password: **123456** |
|       | Video recording from the meeting: [google docs](https://drive.google.com/open?id=1diVd7aCWL1RDAsJjK2uZrE9sRZbccX20) |
| 9:30  | Group morning meetings  - use your virtual group room |
| 9:30  | Hands-on time (you work on exercises) |
| 15:00 | Q&A and feedback |
|       | In the common room, [https://ucldk.zoom.us/join](https://ucldk.zoom.us/join) Meeting ID: **615 3388 1974** Password: **123456**, everybody joins a Q&A + feedback session.
|       | We will talk about experiences during this weeks project and give each other feedback |
|       | Specific agenda will be presented at the meeting |
|       | Video recording from the meeting: [google docs](https://drive.google.com/open?id=1UucE_eBQ6D8PjHVJgPJIGJD66IEfPoZ5) |
| 15:30 | End of day |

### Teacher meeting timetable  

| Time  | itt_project_room1 | itt_project_room2 |
| :---- | :----- | :----- |
| 12:00 | group1 | group6 |
| 12:20 | group5 | group10 |
| 12:40 | group4 | group9 |
| 13:00 | group3 | group8 |
| 13:20 | group2 | group7 |

Please notice that we start at 12.00 (which differs from the norm)


* Virtual rooms for teacher meetings

    We have created two seperate rooms on Zoom for teacher meetings.

    We will message your group on Riot about when you should connect to the room, so please pay attention to Riot and the timetable the entire day.

    For teacher meetings we would like you to have your camera active all the time and your microphone active only when you would like to speak.     

    Connection details for the two rooms are:  

    * **itt_project_room1**  

      To join the video meeting, click this link: [https://ucldk.zoom.us/join](https://ucldk.zoom.us/join) Meeting ID: **639 8472 6817** Password: **123456**

    * **itt_project_room2**

      To join the video meeting, click this link: [https://meet.jit.si/MortensKontor](https://meet.jit.si/MortensKontor)


## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/20s-itt2-project/20S_ITT2_exercises.pdf) for exercises details

## Comments

This week teacher meetings are earlier because nisi has to attend a meeting at 14:00

UCL has a zoom guide: [https://ucl-lte.screenstepslive.com/s/22472/m/93800/l/1252700-participate-in-distance-learning-or-video-conferencing-in-zoom](https://ucl-lte.screenstepslive.com/s/22472/m/93800/l/1252700-participate-in-distance-learning-or-video-conferencing-in-zoom)


* Companion site [https://eal-itt.gitlab.io/iot-with-particle/](https://eal-itt.gitlab.io/iot-with-particle/)

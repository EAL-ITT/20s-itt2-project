---
Week: 24
Content:  part 2 - Make it useful 3/3
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 24

For this last phase of part 2, you are to make your system useful for your users.

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

None at the moment

### Learning goals

None at the moment

## Deliverables

* Daily group morning meeting - see week 05 for agenda
* Mandatory weekly meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
    5. Any other business (AOB)

* Exercise documentation included in Gitlab project

## Schedule


### Monday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Meeting room [https://ucldk.zoom.us/join](https://ucldk.zoom.us/join) Meeting ID: **665 0374 7035** Password: **123456** is open, teachers available as much as possible |
|       | Guide on how to use zoom are in the comments below |
| 9:00  | Introduction to the day, general Q&A session, decide on extra session if applicable |
|       | **During this meeting please keep your camera *enabled* and microphone *disabled*** |
|       | To join the video meeting, click this link: [https://ucldk.zoom.us/join](https://ucldk.zoom.us/join) Meeting ID: **665 0374 7035** Password: **123456** |
|       | Video recording from the meeting: [google docs](https://drive.google.com/open?id=1V7PWOVM5rST7dPZQKFDTiOAsmTO0qETo) |
| 9:30  | Group morning meetings - use your virtual group room |
| 10:00 | Hands-on time (you work on exercises) |
| 12:30 | Teacher meetings - see agenda and timetable |
| 15:00 | Q&A and feedback |
|       | In the common room, [https://ucldk.zoom.us/join](https://ucldk.zoom.us/join) Meeting ID: **665 0374 7035** Password: **123456**, everybody joins a Q&A + feedback session.
|       | We will talk about experiences during this weeks project and give each other feedback |
|       | Specific agenda will be presented at the meeting |
|       | Video recording from the meeting: [google docs](https://drive.google.com/drive/folders/1K5vzi9iwPN92DKymi_4qX3PvQaeG_4yC?usp=sharing) |
| 15:30 | End of day |

### Tuesday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Meeting room [https://ucldk.zoom.us/join](https://ucldk.zoom.us/join) Meeting ID: **665 0374 7035** Password: **123456** is open, teachers probably not available much |
| 9:00  | Introduction to the day, general Q&A session, decide on extra session if applicable |
|       | Topics to be touched upon unittesting, after-easter plan, docs quality comments |    |
|       | **During this meeting please keep your camera *enabled* and microphone *disabled*** |
|       | To join the video meeting, click this link: [https://ucldk.zoom.us/join](https://ucldk.zoom.us/join) Meeting ID: **665 0374 7035** Password: **123456** |
| 9:30  | Group morning meetings  - use your virtual group room |
| 9:30  | Hands-on time (you work on exercises) |
| 12:30 | End-of-project presentations, see exercise for details |
|       | To join the video meeting, click this link: [https://ucldk.zoom.us/join](https://ucldk.zoom.us/join) Meeting ID: **665 0374 7035** Password: **123456** |
|       | Video recording from end-of-project presentations: [google docs](https://drive.google.com/drive/folders/1Ad4PKgEZFufluiGec_TPS5bR2NyDPM0j?usp=sharing) |
| 15:30 | End of day |

### Teacher meeting timetable  

| Time  | itt_project_room1 | itt_project_room2 |
| :---- | :----- | :----- |
| 12:30 | group5 | group10 |
| 12:50 | group4 | group9 |
| 13:20 | group3 | group8 |
| 13:40 | group2 | group7 |
| 14:00 | group1 | group6 |


* Virtual rooms for teacher meetings

    We have created two seperate rooms on Zoom for teacher meetings.

    We will message your group on Riot about when you should connect to the room, so please pay attention to Riot and the timetable the entire day.

    For teacher meetings we would like you to have your camera active all the time and your microphone active only when you would like to speak.     

    Connection details for the two rooms are:  

    * **itt_project_room1**  

      To join the video meeting, click this link: [https://ucldk.zoom.us/join](https://ucldk.zoom.us/join) Meeting ID: **646 2577 7451** Password: **123456**

    * **itt_project_room2**

    To join the video meeting, click this link: [https://ucldk.zoom.us/join](https://ucldk.zoom.us/join) Meeting ID: **689 0024 8703** Password: **938662**

###  End-of-project presentations timetable

| Time  | group  |
| :---- | :----- |
| 12:30 | Introduction |
| 12:35 | group 4 |
| 12:50 | group 3 |
| 13:05 | group 2 |
| 13:20 | break |
| 13:30 | group 1 |
| 13:45 | group 10 |
| 14:00 | group 9 |
| 14:15 | break |
| 14:25 | group 7 |
| 14:40 | group 6 |
| 14:55 | group 5 |
| 15:10 | Round-up and (quick) QA |


## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/20s-itt2-project/20S_ITT2_exercises.pdf) for exercises details

## Comments

* Companion site [https://eal-itt.gitlab.io/iot-with-particle/](https://eal-itt.gitlab.io/iot-with-particle/)

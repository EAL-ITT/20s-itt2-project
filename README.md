![Build Status](https://gitlab.com/EAL-ITT/20s-itt2-project/badges/master/pipeline.svg)


# 20S-ITT2-project

weekly plans, resources and other relevant stuff for the project in IT technology 2. semester spring.

public websites for students:

*  [gitlab pages](https://eal-itt.gitlab.io/20s-itt2-project/)  
*  [students project group](https://gitlab.com/20s-itt-dm-project) 
*  [JTD companion site](https://eal-itt.gitlab.io/iot-with-particle/)

---
Week: 10
tags:
- Documentation
- C++
---

# Exercises for ww10

## Exercise 1 - Automated documentation

### Information

Following up on automated static tests, we expand to include generating documentation using doxygen on push.

### Exercise instructions

Follow the instruction at [https://eal-itt.gitlab.io/iot-with-particle/docs/particle/automated_tests/](https://eal-itt.gitlab.io/iot-with-particle/docs/particle/automated_documentation/)

Make sure to add documentation to all functions and global variables.

## Exercise 2 - Use cpp

### Information

We want to use standard cpp so we are able to use all the cool tools.

### Exercise instructions

1. Convert all .ino files to .cpp.

    Some hints at [https://eal-itt.gitlab.io/iot-with-particle/docs/particle/use_cpp/](https://eal-itt.gitlab.io/iot-with-particle/docs/particle/use_cpp/).

2. Delete the old .ino files

3. Update `.gitlab-ci.yml`

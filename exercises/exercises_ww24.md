---
Week: 24
tags:
- Make it useful
- Documentation
---


# Exercises for ww24

## Exercise 1 - Final project presentation, preparations

In ww22, at the beginning of the "make it useful" part, you decded on goals and what you wanted to present to your user.

The presentation will have your intented users as audience and the purpose for them to understand what your system does and how to start using it. Duration is 15 minutes.

See it as if you were giving an initial introduction to your system to a group of users.

## Instructions

1. In the group, discuss which features and you will be presenting to the users

2. Decide on appropriate presentation technique for each feature

    You will be presenting through zoom, so your users will not be able to touch your device.

    Suggestions are powerpoint for overview combined with live demo (live video and screenshare) of installing and using the system.

3. Create the presentation and write a script/framework for the presentation.

---
Week: 16
tags:
- Proof of concept
- Documentation
---

# Exercises for ww16

## Exercise 1 - Project plan part 2

As we are starting part 2 of the project you need to come up with an idea and a new project plan for what you will be building during part 2.
The period is from now to end of project that is consluded with a presentation Tuesday in week 24.

You need to produce a video ~3 minutes long, that includes a pitch that describes your idea. The video should be uploaded to youtube or similar and shared with teachers and the other project groups. (yes it is okay not to make it public)
We are not experts in video editing software, however a quick google search revealed [https://www.openshot.org/](https://www.openshot.org/) that works for mac, linux and windows.

Your idea needs to be approved by the teachers before you are admitted to proceed, please have this in mind and come up with a good and thorough idea and plan. It is okay to use your idea from week 13's "IoT system design" exercise if it's a good one.

Approval is based on your videos and will take place before next week. We will be present our voting at teacher meetings in week 17. 

Your idea should use the tools and hardware presented in part 1 of the project and should resolve around Internet of things.  
Here's a not exhaustive list for inspiration:

* Hardware status LED
* Monitoring of services
* Interface to other bus industry standard 
* Webservice connecting to REST API
* Integrate particle cloud to e.g. AWS
* etc.

It is adviced to also use the [project plan for students](https://gitlab.com/EAL-ITT/20s-itt2-project/-/blob/master/docs/project_plan_for_students.md) template to describe your idea as you work it out.

## Instructions

1. Brainstorm on your idea
2. Plan for video production
3. Share your video with teachers and other groups on [hackmd](https://hackmd.io/@nisi/ryHU7Sz_U)

Deadline for submitting link to video is today, 2020-04-14. 

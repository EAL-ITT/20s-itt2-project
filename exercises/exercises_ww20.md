---
Week: 20
tags:
- Consolidation
- Documentation
---

# Exercises for ww20

## Exercise 1 - Define consolidation goal

Proof of concept (POC) ended in week 18 and we are in the consolidation phase.

In order for you to structure your work, you must have a clear idea about the goal. This you must define and document.

## Instructions

1. In the group, discuss feature and what you want your system to be able to do at the end of week 21.

2. Makes milestones (and some tasks), diagrams and supporting text to document the system-to-be.

3. Decide what is included in the demo video (and write it down)

    You will be asked to make a video demonstrating your consolidated system in week 21.

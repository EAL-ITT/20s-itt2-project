/*
 * Project serial_2way
 * Description: serial between Boron, node-red and putty
 * Author: NISI
 * Date: 2020-02-12
 */

const unsigned long UPDATE_PERIOD_MS = 1000; //1mS period
unsigned long lastUpdate;
unsigned long counter; 

void setup() {
  // Put initialization like pinMode and begin functions here.
  Serial1.begin(9600);
}

void loop() {
  // Send data in intervals
  if (millis() - lastUpdate >= UPDATE_PERIOD_MS) {
		lastUpdate = millis();
    Serial1.printlnf("sending test data %d", ++counter);
  }

  // if serial data received on Boron rx pin,  print to serial (USB)
  while (Serial1.available() > 0) {
      String incoming = Serial1.readStringUntil('\n');
      Serial.printlnf("received from node-red: " + incoming);  
  }
}
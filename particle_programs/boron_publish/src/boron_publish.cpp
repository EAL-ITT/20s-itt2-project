/******************************************************/
//       THIS IS A GENERATED FILE - DO NOT EDIT       //
/******************************************************/

#include "Particle.h"
#line 1 "c:/Users/localadmin/Documents/EAL-ITT/20s-itt2-project/particle_programs/boron_publish/src/boron_publish.ino"
/*
 * Project boron_publish
 * Description:
 * Author:
 * Date:
 */

// token: 1213101cd4738c5717a052c2e4aadc0977559624 expires Fri Jun 05 2020 20:32:11 GMT+0200
void setup();
void loop();
#line 9 "c:/Users/localadmin/Documents/EAL-ITT/20s-itt2-project/particle_programs/boron_publish/src/boron_publish.ino"
SYSTEM_THREAD(ENABLED);
int counter = 0;
double current = 0;

void setup() {
  Particle.variable("curr", current);
}

void loop() {
  current = counter;
  counter++;
  delay(200);
}